import torch
import torch.nn as nn
import math
import re

class Net(nn.Module):

    def __init__(self, num_cont_parameters, disc_classes_list, lin_layer_sizes, dropout_probs, embedding_multiplicity, n_classes, activation_function, layer_order="LADB", no_batch_norm=False):
        super(Net, self).__init__()

        self.n_classes = n_classes
        self.dropout_probs = dropout_probs
        self.layer_order = layer_order
        self.use_batch_norm = not no_batch_norm

        # Embedding layers
        num_disc_outputs = 0 # Count the outputs of all the embedding layers
        self.embedding_layers = nn.ModuleList()
        for disc_classes in disc_classes_list:  # Loop over all parameters
            num_disc_classes = len(disc_classes)
            if num_disc_classes>1:
                embedding_dims = (num_disc_classes, min(50, math.ceil((num_disc_classes+1)*embedding_multiplicity)) )
                embedding_layer = nn.Embedding(*embedding_dims)
                self.embedding_layers.append(embedding_layer) 
                num_disc_outputs += embedding_dims[1]

        output_size = self.n_classes if self.n_classes>2 else 1         # Signal or background

        ### First layer ###
        # Linear layer both for discrete and continuous parameters
        self.first_linear_layer = nn.Linear(num_disc_outputs + num_cont_parameters, lin_layer_sizes[0])
        self.first_batch_norm_layer = nn.BatchNorm1d(lin_layer_sizes[0])

        ### Linear layers ###
        self.linear_layers = nn.ModuleList()
        self.batch_norm_layers = nn.ModuleList()

        # Hidden linear layers
        for i in range(0, len(lin_layer_sizes)-1):
            linear_layer = nn.Linear(lin_layer_sizes[i], lin_layer_sizes[i+1])
            self.linear_layers.append(linear_layer)

            # Batch normalisation for each hidden layer
            batch_norm = nn.BatchNorm1d(lin_layer_sizes[i+1])
            self.batch_norm_layers.append(batch_norm)

        # Linear Output layer
        self.output_layer = nn.Linear(lin_layer_sizes[-1], output_size)

        ### Activation function
        if activation_function == "ReLU":
            self.activation_function = nn.ReLU()
        elif activation_function == "Sigmoid":
            self.activation_function = nn.Sigmoid()
        elif "LeakyReLU" in activation_function:
            negative_slope = re.findall("\d+\.\d+", activation_function)
            if negative_slope==[]:
                self.activation_function = nn.LeakyReLU()
            else:
                self.activation_function = nn.LeakyReLU(negative_slope=float(negative_slope[0]))
        else: # Use ReLU as default
            self.activation_function = nn.ReLU()            

        ### Dropout layer ###
        self.dropout_first = nn.Dropout(p=self.dropout_probs[0])
        self.dropout_hidden = nn.Dropout(p=self.dropout_probs[1])

    def forward(self, cont_parameters, disc_parameters):
        
        # Embedding layers
        x = [embedding_layer(disc_parameters[:,i].to(torch.long)) for i, embedding_layer in enumerate(self.embedding_layers)]
        x = torch.cat(x, 1)

        # Other layers
        x = torch.cat([x, cont_parameters], 1)
        x = self.first_linear_layer(x)

        if self.layer_order == "LADB":
            x = self.activation_function(x)            
            if self.dropout_probs[0]>0:
                x = self.dropout_first(x)
            if self.use_batch_norm:
                x = self.first_batch_norm_layer(x)

        elif self.layer_order == "LABD":
            x = self.activation_function(x)
            if self.use_batch_norm:            
                x = self.first_batch_norm_layer(x)
            if self.dropout_probs[0]>0:
                x = self.dropout_first(x)

        elif self.layer_order == "LBAD":
            if self.use_batch_norm:            
                x = self.first_batch_norm_layer(x)
            x = self.activation_function(x)
            if self.dropout_probs[0]>0:
                x = self.dropout_first(x)

        elif self.layer_order == "LDAB":
            if self.dropout_probs[0]>0:
                x = self.dropout_first(x)
            x = self.activation_function(x)
            if self.use_batch_norm:
                x = self.first_batch_norm_layer(x)

        elif self.layer_order == "LDBA":
            if self.dropout_probs[0]>0:
                x = self.dropout_first(x)
            if self.use_batch_norm:
                x = self.first_batch_norm_layer(x)
            x = self.activation_function(x)

        # Hidden layers
        for (linear_layer, batch_norm_layer) in zip(self.linear_layers, self.batch_norm_layers):
            x = linear_layer(x)
    
            if self.layer_order == "LADB":
                x = self.activation_function(x)
                if self.dropout_probs[1]>0:
                    x = self.dropout_hidden(x)
                if self.use_batch_norm:                    
                    x = batch_norm_layer(x)

            elif self.layer_order == "LABD":
                x = self.activation_function(x)
                if self.use_batch_norm:                
                    x = batch_norm_layer(x)
                if self.dropout_probs[1]>0:
                    x = self.dropout_hidden(x)

            elif self.layer_order == "LBAD":
                if self.use_batch_norm:                
                    x = batch_norm_layer(x)
                x = self.activation_function(x)
                if self.dropout_probs[1]>0:
                    x = self.dropout_hidden(x)

            elif self.layer_order == "LDAB":
                if self.dropout_probs[1]>0:
                    x = self.dropout_hidden(x)
                x = self.activation_function(x)
                if self.use_batch_norm:                
                    x = batch_norm_layer(x)

            elif self.layer_order == "LDBA":
                if self.dropout_probs[1]>0:
                    x = self.dropout_hidden(x)
                if self.use_batch_norm:                    
                    x = batch_norm_layer(x)
                x = self.activation_function(x)

        x = self.output_layer(x)

        return x

    def weight_reset(m):
        reset_parameters = getattr(m, "reset_parameters", None)
        if callable(reset_parameters):
            m.reset_parameters()
