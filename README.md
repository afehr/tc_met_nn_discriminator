# tc+MET Neural Net Discriminator

This is the neural network discriminator for the tc+MET 136 fb-1 ATLAS analysis.

## Input arguments

### Inputs tensors (if stored)
```
"--sn_tensor", help="Signal tensor file (.pt, defaut: dataset_bg_list.pt)", default="dataset_sn_list.pt"
"--bg_tensor", help="Background tensor file (.pt, default: dataset_bg_list.pt)", default="dataset_bg_list.pt"
```
### List of input root files
```
"-s", "--signal_files", help="Signal files (ROOT)", nargs="+", required=True            # Take one or more signal root files
"-b", "--background_files", help="Background files (ROOT)", nargs="+", required=True    # Take one or more background root files
```
### How many inputs events should be used?
```
"--n_events_sn", help="Number of signal events to be used in total (training + validation)", required=True, type=int
"--n_events_bg", help="Number of background events to be used in total (training + validation)", required=True, type=int
```
### Hyperparameters
```
"-e", "--n_epochs", help="Number of epochs", type=int, required=True
"-n", "--batch_size", help="Batch size (default: 4)", type=int, default=64
"-l", "--lin_layer_sizes", help="Sizes of linear layers (e.g '20 40' )", nargs="+", type=int, required=True
"-d", "--dropout_prob", help="Probability of dropout in dropout layers (default: 0.3)", type=float, default=0.3
"-m", "--embedding_multiplicity", help="Define embedding multiplicity (default: 0.5, this would mean that n discrete values are embedded into min. (n+1)*0.5 continuous parameters", type=float, default=0.5
"--split", help="Split between train and validation set (default: 0.5)", type=float, default=0.5
"--learning_rate", help="learning_rate, see https://pytorch.org/docs/stable/optim.html#torch.optim.SGD (default: 0.001)", type=float, default=0.001
"--momentum", help="momentum, see https://pytorch.org/docs/stable/optim.html#torch.optim.SGD (default: 0.9)", type=float, default=0.9
```
### Outputs
```
"-o", "--output_file", help="Output file (.txt)", default="nn_results.txt"
"-w", "--write_discriminator", help="Enable writing of discriminator to n_tuples", action="store_true"
```
### Code run arguments
```
"-q", "--quiet", help="Suppress showing plots", action="store_true"
"-g", "--gpu", help="Allow to use gpu if available", action="store_true"
```

## Run the neural net code locally

### Install the necessary conda packages
```
conda create --name TORCH
conda activate TORCH
PYTHON_VERSION=3.7
conda install -y python=$PYTHON_VERSION
conda install -y pytorch torchvision cudatoolkit=10.1 -c pytorch
conda install -c conda-forge uproot
conda install -c conda-forge root
conda install scikit-learn
conda install -c anaconda pandas
```

### On each login
conda activate TORCH

### Run the code locally (example)
```
python Classifier.py -s ntuples/mc16_13TeV.437072.MGPy8EG_A14N23LO_BB_direct_1300_400_MET150.e7417_a875_r10201_p3990_bbMeT.root -b ntuples/mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.e5271_s3126_r9364_p3990_bbMeT.root --n_epochs 100 -l 40 40 --n_events_sn 2786 --n_events_bg 5000
```


## Building and using docker image

### Build and push the docker image:
Build the docker image from repository folder:
```
cd tc_met_nn_discriminator
docker build -t gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> . # Currently used tag is withTensors
```
Push the docker image to the [container registry](https://gitlab.cern.ch/afehr/tc_met_nn_discriminator/container_registry) of this repository on gitlab:
```
docker push gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG>
```

### Pull the docker image onto any device, always do this when docker image has changed
Pull the docker image from the [gitlab container registry](https://gitlab.cern.ch/afehr/tc_met_nn_discriminator/container_registry):
```
docker pull gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG>
```


### Run the docker image on any device
Pull the docker image (to keep it updated):
```
docker pull gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG>
```
Run the docker image using the cpu:
```
docker run --rm -ti --ipc=host gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> python /app/Classifier.py <NN arguments>
```
Example using the tensor files included in the container:
```
docker run --rm -ti --ipc=host gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> python /app/Classifier.py -sn_tensor /app/data/475_300.pt -bg_tensor /app/data/Z+jets.pt --n_epochs 100 -l 40 40 --n_events_sn 2786 --n_events_bg 5000
```
In order to run the docker container on a GPU, do the following:
```
docker run --gpus all --rm -ti --ipc=host gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> python /app/Classifier.py --gpu <NN arguments>
```


## Run the docker image in singularity

Pull the docker image
```
singularity pull docker://gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG>
```
Run the Classifier.py script
```
singularity exec docker://gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> python /app/Classifier.py <NN arguments>
```
Example running on the included .pt files:
```
singularity exec docker://gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> python /app/Classifier.py -sn_tensor /app/data/475_300.pt -bg_tensor /app/data/Z+jets.pt --n_epochs 100 -l 40 40 --n_events_sn 2786 --n_events_bg 5000
```


## Run the docker image using prun on the grid
Run it with grid CPUs:
```
prun --containerImage docker://gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> --exec "python /app/Classifier.py <NN arguments>" --noBuild --tmpDir /tmp --outDS <outDS_name>
```

Run it with grid GPUs (on the Manchester grid site that has GPUs):
```
prun --containerImage docker://gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> --exec "python /app/Classifier.py <NN arguments> --gpu" --noBuild --tmpDir /tmp --outDS <outDS_name> --cmtConfig nvidia-gpu --site ANALY_MANC_GPU_TEST
```


## Run the docker image with local files
To run the docker image on local files, add the following to the docker command:
Run the docker image, mounting the directory where the signal and background nTuples are locally stored:
```
--mount src=<Absolute local path to root files>,target=/app/dataLoc,type=bind
```
The files are then available under ```/app/dataLoc``` in the docker image. Here is an example run command:
```
docker run --rm -ti --mount src=/Users/armin/tc_met_nn_discriminator/ntuples,target=/app/dataLoc,type=bind --ipc=host gitlab-registry.cern.ch/afehr/tc_met_nn_discriminator:<TAG> python /app/Classifier.py -sn_tensor /app/data/475_300.pt -bg_tensor /app/data/Z+jets.pt --n_epochs 100 -l 40 40 --n_events_sn 2786 --n_events_bg 5000
```
One can also use mount to store the output file(s) locally:
```
--mount src=<Absolute local path where output files should be stored>,target=/app/dataLoc,type=bind -o ./dataLoc/nn_results.txt
```