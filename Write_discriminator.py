# Loading basics
import sys, argparse
import numpy as np
from time import sleep
from array import array

# Loading additonal libraries
import time                         # To measure run times
import os                           # For output file to check if it is empty
import logging
import logging.config

### Logging ###
logging.config.fileConfig('logging.conf', defaults={'logfilename': "Write_discriminator.log"})
global logger
logger = logging.getLogger('myLogger')

# Loading torch
import torch
from torch.utils.data import DataLoader
from RootToDataset import RootToDataset
from Net import Net
from Classifier import read_ntuple, construct_dataset, get_device
import ROOT 

def parse_arguments(argv=None):

    parser = argparse.ArgumentParser()

    # Inputs tensors to load/store
    parser.add_argument("-i", "--ntuples", help="Input ntuples (.root files)", nargs="*", required=True)                    # Take one or more signal tensors
    parser.add_argument("-m", "--model_folder", help="The folder, where the model to be used for calculating the NN output score is saved in as well as the parameters (default: this folder)", default="")
    parser.add_argument("-n","--name_branch", help="Name of branch to be written (default: NNscore)", default="NNscore")

    # Code run arguments
    parser.add_argument("-g", "--gpu", help="Allow to use gpu if available", action="store_true")
    parser.add_argument("--threads", help="How many threads a job is allowed to use (default: 1).", type=int, default=1)
    parser.add_argument("--num_workers", help="num_workers for DataLoader (default: 0)", type=int, default=0)

    parser.add_argument("--tree_names", help="Tree names to be written (default: Nominal)", nargs="*", default="Nominal")

    # Hack to replace variable names, Jet -> JetLep 
    parser.add_argument("--replace_parameters", help="Hack to replace variable names, e.g replacing Jet by JetLep: 'Jet,JetLep' (default: ''", nargs="*", default=None)

    return parser.parse_args(argv)


def main(args):

    ### Limit threads for the job
    num_threads = args.threads
    torch.set_num_threads(num_threads)
    logger.info("Used Threads:" + str(torch.get_num_threads()))

    ### Get GPU if available, otherwise take CPU ###
    device = get_device(args.gpu)

    ### Write discriminator and NN to all input ntuples ###
    discriminator_write_time = time.time()
   
    ### Load model, continuous and discrete parameters ###
    net, optimizer, class_names = torch.load(args.model_folder + "model.pt")
    net.eval()  # Sets the NN into validation mode, not using dropout layers and batch normalisation
    continuous_parameters, means, stds = torch.load(args.model_folder + "continuous_parameters.pt")
    discrete_parameters, disc_classes_list, is_missing_strs, ignored_values = torch.load(args.model_folder + "discrete_parameters.pt")

    # Replace variable strings in continuous and discrete parameters .pt files
    if args.replace_parameters!=None:
        logger.info("Replacement of input parameter names taking place due to --replace_parameters !!!")
        for i in range(int(len(args.replace_parameters)/2)):
            origStr = args.replace_parameters[2*i]
            newStr = args.replace_parameters[2*i+1]
            logger.info("Replacing input parameter names! Replacing " + origStr + " by " + newStr)
            discrete_parameters = [par.replace(origStr, newStr) for par in discrete_parameters]
            continuous_parameters = [par.replace(origStr, newStr) for par in continuous_parameters]       
            is_missing_strs = [par.replace(origStr, newStr) for par in is_missing_strs]            
    logger.info(discrete_parameters)
    logger.info(disc_classes_list)
    logger.info(is_missing_strs)
    logger.info(ignored_values)   

    for i, ntuple in enumerate(args.ntuples):
        ### Fill tree
        f = ROOT.TFile(ntuple,"update")
        for tree_name in args.tree_names:
            logger.info(tree_name)
            time_write =  time.time()
            logger.info("Writing input file %s, %d out of %d" % (ntuple, i, len(args.ntuples)))
            input_data = read_ntuple(ntuple, continuous_parameters, [s for s in discrete_parameters if "_is_missing" not in s], None, apply_preselection=False, tree_name=tree_name)
            if input_data==None:
                logger.warning("Tree " + tree_name + " in file either has 0 events or has an issue")
                continue
            dataset = construct_dataset([input_data], 0, continuous_parameters, [s for s in discrete_parameters if "_is_missing" not in s], device, class_names)
    
            # Apply normalisation and class encoding
            dataset.apply_normalisation(means, stds, ignored_values)
            unused = dataset.replace_missing_data(means, ignored_values=ignored_values, default_missing_strs=is_missing_strs)
            dataset.set_disc_values(disc_classes_list)
            try:
                dataset.replace_not_trained_values()    # Replace values that only occur in events that didn't pass pre-selection by last value of disc variable        
            except:
                logger.error("File %s is not working, probably no event passes pre-selection, check this!" % ntuple)
                continue
    
            dataset.apply_class_encoding(is_missing_strs)
    
            # Define data loader
            loader_write_ntuple = torch.utils.data.DataLoader(dataset, batch_size = len(dataset), num_workers = args.num_workers, shuffle=False, pin_memory=False)
    
            with torch.no_grad():
                probabilities = None
                # Calculate the discriminator
                for sample in loader_write_ntuple:
                    cont_parameters_batch, disc_parameters_batch = sample[1], sample[2]
                    optimizer.zero_grad()
                
                    output = net(cont_parameters_batch, disc_parameters_batch)
                    if len(class_names)==2:
                        probabilities = np.transpose(output)[0].cpu()
                        probabilities = torch.sigmoid(probabilities)                        
                    else:
                        probabilities = output.cpu()
                
                branch_names = list()
                if len(class_names)==2:
                    branch_names.append(args.name_branch)
                else:
                    for class_name in class_names:
                        branch_names.append(str(args.name_branch + class_name).replace("+", "").replace(" ","").replace("_",""))
    
                weight = array('f', [0.])

                ### Fill tree
                T = f.Get(tree_name)                
                for i_branch, branch_name in enumerate(branch_names):
                    NNOutputScoreBranch = T.Branch(branch_name, weight, branch_name + "/F")
                    n_events = T.GetEntries()
                    for i in range(n_events):
                        if (i%10000 == 0):
                            logger.debug("Branch + " + branch_name + " current Event being Processed is: " + str(i))
                        T.GetEntry(i)
                        weight[0] = float(probabilities[i]) if len(class_names)==2 else float(probabilities[i][i_branch])
                        NNOutputScoreBranch.Fill()
                    T.Write("", ROOT.TObject.kOverwrite)  
            del loader_write_ntuple, dataset, input_data
            logger.info("Writing of tree " + tree_name + " took " + str(time.time()-time_write) + " s")
        f.Close()            
    return 0

if __name__ == "__main__":
    # Parse arguments
    args = parse_arguments(sys.argv[1:])

    # Go to main function
    main(args)
