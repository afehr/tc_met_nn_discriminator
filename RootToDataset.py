import torch
import numpy as np
from torch.utils.data import Dataset
from torch._utils import _accumulate
from torch import randperm, default_generator

from sklearn import preprocessing       # For LabelEncoder for discrete parameters
import logging

class RootToDataset(Dataset): # Inherit from Dataset in order to be able to add datasets

    def __init__(self, labels, cont_parameters, disc_parameters, cont_parameters_strs, disc_parameters_strs, disc_values_list, device, transform=None):
        self.labels = labels.to(device)
        self.cont_parameters_strs = cont_parameters_strs
        self.disc_parameters_strs = disc_parameters_strs
        self.cont_parameters = cont_parameters.to(device)
        self.disc_parameters = disc_parameters.to(device)
        self.disc_values_list = disc_values_list
        self.transform = transform
        self.device = device

    def __len__(self):
        return len(self.cont_parameters)


    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        labels = self.labels[idx]
        cont_parameters = self.cont_parameters[idx,0:]
        disc_parameters = self.disc_parameters[idx,0:]

        if self.transform:
            sample = self.transform(sample)

        return labels, cont_parameters, disc_parameters

    def __add__(self, other):
        labels = torch.cat([self.labels, other.labels], dim=0)
        cont_parameters_strs = self.cont_parameters_strs
        disc_parameters_strs = self.disc_parameters_strs
        cont_parameters = torch.cat([self.cont_parameters, other.cont_parameters], dim=0)
        disc_parameters = torch.cat([self.disc_parameters, other.disc_parameters], dim=0)
        disc_values_list = list()
        for par_self, par_other in zip(self.disc_values_list, other.disc_values_list):
            disc_values_list.append(np.unique(np.concatenate((par_self, par_other), axis=0)))
        return RootToDataset(labels, cont_parameters, disc_parameters, cont_parameters_strs, disc_parameters_strs, disc_values_list, self.device)

    def __radd__(self, other):
        if other == 0:
            return self
        else:
            return self.__add__(other)

    def random_split(self, lengths, generator=default_generator):
        if sum(lengths) != len(self):
            raise ValueError("Sum of input lengths does not equal the length of the input dataset!")
        
        datasets = list()
        indices = torch.randperm(sum(lengths), generator=generator).tolist()
        for offset, length in zip(_accumulate(lengths), lengths):
            labels = self.labels[indices[offset - length:offset]]
            cont_parameters_strs = self.cont_parameters_strs
            disc_parameters_strs = self.disc_parameters_strs
            cont_parameters = self.cont_parameters[indices[offset - length:offset]]
            disc_parameters = self.disc_parameters[indices[offset - length:offset]]
            disc_values_list = self.disc_values_list
            device = self.device
            dataset = RootToDataset(labels, cont_parameters, disc_parameters, cont_parameters_strs, disc_parameters_strs, disc_values_list, device)
            datasets.append(dataset)
        return [dataset for dataset in datasets] 

    def get_normalisation(self, ignored_values=None):
        means, stds = np.zeros(len(self.cont_parameters_strs)), np.zeros(len(self.cont_parameters_strs))
        for i_parameter, parameter in enumerate(self.cont_parameters_strs):
            # Ignore defaut values
            cont_parameter = self.cont_parameters[:, i_parameter]
            for ignored_value in ignored_values:
                cont_parameter = cont_parameter[cont_parameter != ignored_value]

            # Calculate mean and standard derivation
            means[i_parameter] = torch.mean(cont_parameter)
            stds[i_parameter] = torch.std(cont_parameter)

        return means, stds

    def replace_missing_data(self, means, ignored_values=list(), default_missing_strs=list()):
        is_missing_data_list = list()

        # Check for missing data
        for i_parameter, parameter in enumerate(self.cont_parameters_strs):
            cont_parameter = self.cont_parameters[:, i_parameter]
            is_missing_tmp = torch.LongTensor(len(cont_parameter)).zero_()   # temporary array to store if a value was missing
    
            # If a value of a parameter should be ignored, it is replaced with the mean of that parameter
            for ignored_value in ignored_values:
                is_missing_tmp[cont_parameter==ignored_value] = 1
                cont_parameter[cont_parameter==ignored_value] = means[i_parameter]

            is_missing_data = is_missing_tmp.type(torch.LongTensor).unsqueeze(-1)
            is_missing_str = parameter + "_is_missing"
            is_missing_count = torch.sum(is_missing_tmp).numpy()

            if parameter in [default_missing_str.replace("_is_missing","") for default_missing_str in default_missing_strs]:
                is_missing_data_list.append(is_missing_data)
                self.disc_parameters = torch.cat([self.disc_parameters.type(torch.LongTensor),is_missing_data], dim=1)
                self.disc_values_list.append(np.unique(is_missing_data))
                self.disc_parameters_strs.append(is_missing_str)
    
            elif is_missing_count>0:
                is_identical = False
                for is_missing_data_i in is_missing_data_list:
                    if torch.all(is_missing_data.eq(is_missing_data_i)):
                        is_identical=True

                # Add is_missing discrete parameters if is_missing contains unique data (e.g no etaJet4_is_missing is needed if there is already ptJet4_is_missing, as they have identical values)
                if not is_identical:
                    is_missing_data_list.append(is_missing_data)
                    self.disc_parameters = torch.cat([self.disc_parameters.type(torch.LongTensor),is_missing_data], dim=1)
                    self.disc_values_list.append(np.unique(is_missing_data))
                    self.disc_parameters_strs.append(is_missing_str)
        return self.disc_parameters_strs 

    def apply_normalisation(self, means, stds, ignored_values=None):   # Normalise continuous parameter
        for i_parameter, parameter in enumerate(self.cont_parameters_strs):
            cont_parameter = self.cont_parameters[:, i_parameter]
            means_tensor = torch.Tensor(len(cont_parameter)).fill_(means[i_parameter])
            
            for ignored_value in ignored_values:
                cont_parameter = torch.where(cont_parameter==ignored_value, means_tensor, cont_parameter) 
            cont_parameter = torch.Tensor(cont_parameter-means[i_parameter])/stds[i_parameter] # Normalise to mean of 0 and standard deviation of 1
            self.cont_parameters[:, i_parameter] = cont_parameter

    def get_disc_values(self):
        return self.disc_values_list

    def set_disc_values(self, disc_values_list):
        self.disc_values_list = disc_values_list

    def apply_class_encoding(self, is_missing_strs=[]):
        # Label encoder for one-hot encoding
        label_encoder = preprocessing.LabelEncoder()

        # Temporary new tensors
        disc_parameters_list = list()
        disc_parameters_strs = list()

        for i_parameter, (disc_values, disc_parameter_str) in enumerate(zip(self.disc_values_list, self.disc_parameters_strs)):
            label_encoder.fit(disc_values)  # Use classes that get_disc_values can give

            if len(label_encoder.classes_) > 1 or disc_parameter_str + "_is_missing" in is_missing_strs:
                # Shift to integer class labels from 0 to n_classes-1
                disc_parameter = torch.LongTensor(len(self.disc_parameters), len(self.disc_parameters_strs)).zero_()
                disc_parameter = torch.from_numpy( label_encoder.fit_transform(self.disc_parameters[:, i_parameter]) ).unsqueeze(-1)
                disc_parameters_list.append(disc_parameter)
                disc_parameters_strs.append(disc_parameter_str)

        self.disc_parameters = torch.cat(disc_parameters_list, dim=1)
        self.disc_parameters_strs = disc_parameters_strs
    
        logging.debug("Discrete parameters that have more than one possible value: " + str(self.disc_parameters_strs))

        return self.disc_parameters_strs

    def shuffle_parameter(self, shuffle_parameter_strs):
        
        for i, parameter_str in enumerate(self.cont_parameters_strs):
            if parameter_str in shuffle_parameter_strs:
                indices = torch.randperm(len(self.cont_parameters[:, i]))
                parameter_new = torch.index_select(self.cont_parameters[:, i], 0, indices)
                self.cont_parameters[:, i] = parameter_new.to(self.device)

        for i, parameter_str in enumerate(self.disc_parameters_strs):
            if parameter_str in shuffle_parameter_strs:
                indices = torch.randperm(len(self.disc_parameters[:, i]))
                parameter_new = torch.index_select(self.disc_parameters[:, i], 0, indices)
                self.disc_parameters[:, i] = parameter_new.to(self.device)

    # Function to replace values that only occur in events that were cut in pre-selection. This is necessary as otherwise, the NN encounters new discrete parameter values when it is used to write the NN score to the ntuples
    def replace_not_trained_values(self):
        for i, disc_values in enumerate(self.disc_values_list):
            tensor = self.disc_parameters[:, i] 
            tensor = torch.where(torch.from_numpy(np.in1d(tensor, disc_values)), tensor, torch.LongTensor([int(disc_values[-1])]))
#            self.disc_parameters[:, i] = tensor
