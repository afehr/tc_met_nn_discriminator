### Needed conda packages ###
# conda install -c pytorch pytorch
# conda install -c conda-forge uproot
# conda install -c conda-forge root
# conda install scikit-learn
# conda install -c anaconda pandas

# Loading basics
import sys, argparse, copy, glob
import numpy as np
import math
import subprocess       # To run bash commands in python
from datetime import datetime   # To get current time
import json 
import pandas as pd

# Loading Root tools from uproot
import uproot
uproot.default_library = "np"

# Loading additonal libraries
import time                         # To measure run times
import os                           # For output file to check if it is empty
from sklearn.model_selection import StratifiedKFold

# Loading torch
import torch
from torch.utils.data import DataLoader
from RootToDataset import RootToDataset
from Net import Net

# Load root for plotting
import ROOT
from ROOT import gROOT, TCanvas, TH1D, TColor, TPad, TLegend, TGraph, TLatex, TLine, TFile, TMultiGraph
gROOT.SetStyle("ATLAS")
colors = [ROOT.kRed, ROOT.kBlue, ROOT.kOrange, ROOT.kViolet, ROOT.kGreen, ROOT.kCyan, ROOT.kYellow, ROOT.kBlack]
linestyles = [ROOT.kSolid, ROOT.kDashed, ROOT.kDotted]
markerstyles = [ROOT.kFullCircle, ROOT. kOpenSquare, ROOT.kFullTriangleUp]

### Logging ###
import logging
import logging.config
global logger
logger = logging.getLogger('myLogger')

ignored_values = [-99.0, -999.0]    # Values in continuous parameters to be ignored (default values)

# Additional parameters used in pre-selection
pre_selection_par_strs = ["nbaselineLep", "passMETtriggers", "dphimin3", "NCTag", "NBTag", "metSigST", "MTcMin", "met", "NJet", "tcMeTCategory", "isBTagJet1", "isCTagJet1"]

### Parse arguments
def parse_arguments(argv=None):
    
    parser = argparse.ArgumentParser()

    # List of input root files
    parser.add_argument("-i", "--input_json", help="Input json file with categories and txt file with root files", type=argparse.FileType('r'), required=True) 

    # How many inputs events should be used?
    parser.add_argument("--split", help="Split between training and testing set (default: 0.2), the training set will further be split into training and validation as defined by nFolds", type=float, default=0.2)

    parser.add_argument("--continuous_pars", help="List of continuous parameters to be used (default is all: ptJet1 ptJet2 ptJet3 ptJet4 ptJet5 ptJet6 etaJet1 etaJet2 etaJet3 etaJet4 etaJet5 etaJet6 dPhiJet1 dPhiJet2 dPhiJet3 dPhiJet4 dPhiJet5 dPhiJet6 met metSigST MTcMin MTbMin dRj1j2 dRb1c1 meff MTjMin mctbc mctjj ptBJet1 ptCJet1 etaBJet1 etaCJet1 dPhiBJet1 dPhiCJet1 ptBJet2 ptCJet2 etaBJet2 etaCJet2 dPhiBJet2 dPhiCJet2 MT2bInRC MTbMax MTcMax MTjMinClose MTbMinClose MTcMinClose mRc12Jet1 mRc12Jet2 mRc12JetMin mRc12JetMax nDNNTop_fatjet_kt10 MT2", nargs="*", default=["ptJet1", "ptJet2", "ptJet3", "ptJet4", "ptJet5", "ptJet6", "etaJet1", "etaJet2", "etaJet3", "etaJet4", "etaJet5", "etaJet6", "dPhiJet1", "dPhiJet2", "dPhiJet3", "dPhiJet4", "dPhiJet5", "dPhiJet6", "met", "metSigST", "MTcMin", "MTbMin", "dRj1j2", "dRb1c1", "meff", "MTjMin", "mctbc", "mctjj", "ptBJet1", "ptCJet1", "etaBJet1", "etaCJet1", "dPhiBJet1", "dPhiCJet1", "ptBJet2", "ptCJet2", "etaBJet2", "etaCJet2", "dPhiBJet2", "dPhiCJet2", "MT2bInRC", "MTbMax", "MTcMax", "MTjMinClose", "MTbMinClose", "MTcMinClose", "mRc12Jet1", "mRc12Jet2", "mRc12JetMin", "mRc12JetMax", "nDNNTop_fatjet_kt10", "MT2"]) 
    parser.add_argument("--discrete_pars", help="List of discrete parameters to be used (default is all: NJet NBTag NCTag isBTagJet2 isCTagJet2 isBTagJet3 isCTagJet3 isBTagJet4 isCTagJet4 isBTagJet5 isCTagJet5 isBTagJet6 isCTagJet6 flavJet2 flavJet3 flavJet4 flavJet5 flavJet6", nargs="*", default=["NJet", "NBTag", "NCTag", "isBTagJet2", "isCTagJet2", "isBTagJet3", "isCTagJet3", "isBTagJet4", "isCTagJet4", "isBTagJet5", "isCTagJet5", "isBTagJet6", "isCTagJet6", "flavJet2", "flavJet3", "flavJet4", "flavJet5", "flavJet6"])

    # Hyperparameters: NN architecture and batch settings
    parser.add_argument("-e", "--n_epochs", help="Number of epochs (default: 100)", type=int, default=100)
    parser.add_argument("-n", "--batch_size", help="Batch size (default: 64)", type=int, default=64)
    parser.add_argument("-l", "--lin_layer_sizes", help="Sizes of linear layers (e.g '20 40' )", nargs="+", type=int, required=True)
    parser.add_argument("-d", "--dropout_probs", help="Probability of dropout in dropout layers [first layer, hidden layers] (default: [0.2,0.2])", nargs="+", type=float, default=[0.2,0.2])
    parser.add_argument("-m", "--embedding_multiplicity", help="Define embedding multiplicity (default: 0.5, this would mean that n discrete values are embedded into min. (n+1)*0.5 continuous parameters", type=float, default=0.5)
    parser.add_argument("--activation_function", help="Activation function to be used, either Sigmoid, ReLU or LeakyReLU <negative_slope>", default="ReLU")

    # Hyperparameters: Learning behaviour 
    parser.add_argument("--optimizer", help="Choose optimizer to use, options are: Adam, SGD (default: Adam)", default="Adam")
    parser.add_argument("--learning_rate", help="learning_rate, see https://pytorch.org/docs/stable/optim.html#torch.optim.SGD (default: 0.001)", type=float, default=0.001)
    parser.add_argument("--momentum", help="momentum, see https://pytorch.org/docs/stable/optim.html#torch.optim.SGD (default: 0.9)", type=float, default=0.9)
    parser.add_argument("--weight_decay", help="weight decay of Adam optimizer (default: 0.0)", type=float, default=0.0)    
    parser.add_argument("--early_stopping_epochs", help="Number of epochs to wait for a decreasing loss (default: 9999)", type=int, default=9999)

    # Outputs
    parser.add_argument("-o", "--output_file", help="Output file (.txt)", default="nn_results.csv")
    parser.add_argument("-sf", "--save_folder", help="The folder, the plots will get saved in (default: same folder as program)", default="results")

    # Code run arguments
    parser.add_argument("-q", "--quiet", help="Suppress showing plots", action="store_true", default=True)
    parser.add_argument("-g", "--gpu", help="Allow to use gpu if available", action="store_true")
    parser.add_argument("--threads", help="How many threads a job is allowed to use (default: 1).", type=int, default=1)
    parser.add_argument("--num_workers", help="num_workers for DataLoader (default: 0)", type=int, default=0)

    # Optionally supply model -> Only doing validation
    parser.add_argument("--model", help="Optionally supply model to do validation on, no training is done", default="")
    
    parser.add_argument("--nFolds", help="Number of k-folds to be used, use '1' for not doing k-fold cross validation with a 80-20 training-validation split (if you want to change the 80-20, then modify the StratifiedKFold instantiation in the source code), default: 5", type=int, default=5) 

    parser.add_argument("--layer_order", help="Order of layers, possibilities: LADB LABD LBAD LDAB LDBA", default="LADB")
    parser.add_argument("--no_batch_norm", help="Whether to use batch_norm layers, (default: True)", action="store_true")
    return parser.parse_args(argv)


### Get computing device (cpu or gpu)
def get_device(gpu):
    device = torch.device("cpu")
    if gpu:
        try:
            logger.info("Trying to use GPU, looking for available cuda device: ")
            subprocess.run("nvidia-smi")        # If this command gives an output, CUDA should be installed
            if torch.cuda.is_available():
                device = torch.device("cuda:0")
                torch.backends.cudnn.benchmark = True
                torch.backends.cudnn.enabled = True
            else:
                logger.critical("torch.cuda.is_available() is FALSE -> aborting")
                exit(1)
        except:
            logger.critical("nvidia-smi command not found -> CUDA not installed on resource -> aborting")
            exit(1)

    logger.info("Using device " + str(device))
    

### Load content of json file
def load_json_input(input_json, continuous_par_strs, discrete_pars, pre_selection_par_strs):
    input_classes = list()
    for json_input in json.load(input_json):
        event_types = list()
        input_class = {
            "name": json_input["class_name"],
            "weight": json_input["weight"],
            "event_types": event_types
        }

        # Loop over all event_types of this class
        for item in json_input["event_types"]:

            # Define event_type
            event_type = {
                "name": item["name"],
                "n_events": item["n_events"],
                "tensor_files": item["tensor_files"],
            }

            # Load data
            root_files = list()
            txt_files = item["txt_files"].split()
            logger.debug("from .txt file " + str(txt_files) + ": ")
            for txt_file in txt_files:
                with open(txt_file) as f:
                    patterns = [line.rstrip() for line in f]
                    for pattern in patterns:
                        files = glob.glob(pattern)
                        logger.debug("Pattern " + pattern + ":\n" + str(files))
                        for root_file in files:
                            root_files.append(str(root_file))
                            logger.debug(root_file)

            logger.info("\nLoading %s event data of class %s" % (event_type["name"], input_class["name"]))
            data = load_data(event_type["tensor_files"], root_files, continuous_par_strs, discrete_pars, pre_selection_par_strs)

            # Add data to event_type
            event_type["data"] = data

            event_types.append(event_type)

        input_classes.append(input_class)
    return input_classes


### Load .pt files if saved in previous iteration or read ntuples 
def load_data(tensor_file, root_files, continuous_par_strs, discrete_par_strs, pre_selection_par_strs):
    inputs_list = list()

    ### Read data from root files is no tensor_file is set
    if tensor_file is "":
        # Get input
        for i, root_file in enumerate(root_files):
            logger.debug("Reading file %d out of %d: %s" % (i+1, len(root_files), root_file))
            input = read_ntuple(root_file, continuous_par_strs, discrete_par_strs, pre_selection_par_strs)
            if input!=None:
                inputs_list.append(input)
    ### Try to read tensor files, reload data from root files if tensor file doesn't exist
    else:
        logger.info("Tensor file: " + tensor_file)
        try:
            inputs = torch.load(tensor_file)
            for input in inputs:
                inputs_list.append(input)
        except:
            logger.info("\nNo data found, reloading it now...")

            # Get Signal input
            for i, root_file in enumerate(root_files):
                logger.debug("Reading file %d out of %d: %s" % (i+1, len(root_files), root_file))
                input = read_ntuple(root_file, continuous_par_strs, discrete_par_strs, pre_selection_par_strs)
                if input!=None:
                    inputs_list.append(input)

            torch.save(inputs_list, tensor_file) # Save data to disk
    logger.info("Number of root files considered: " + str(len(inputs_list)))
    return inputs_list


### Read n_tuples
def read_ntuple(root_file, continuous_par_strs, discrete_par_strs, pre_selection_par_strs, apply_preselection=True, tree_name="Nominal"):

    par_strs = list()
    for par in continuous_par_strs + discrete_par_strs:
        par_strs.append(par)

    # Make sure parameters used only for pre-selection are also loaded
    if apply_preselection:
        for par in pre_selection_par_strs:
            if par not in continuous_par_strs + discrete_par_strs:
                par_strs.append(par)
    
    # Reading tree from root file, fails if root file contains 0 events
    events = None
    try:
        # Get tree name automatically (assuming it's the first tree!!!)
        logger.debug("Using tree: " + tree_name)

        # Read using uproot
        events = uproot.concatenate(root_file + ":" + tree_name, par_strs)
        # file[tree_name].show() # Show contents of tree
    except:
        logger.warning("File " + root_file + " not working or 0 events in file, ignoring it!")
        return None
  
    # Get unique values of discrete parameters 
    disc_values = list()
    for par in discrete_par_strs:
        disc_values.append(np.unique(np.array(events[par])))

    # Apply pre-selection. Used variables must be stated in pre_selection_par_strs! 
    if apply_preselection:
        logger.debug("n_events before pre-selection: " + str(len(events)))
        try:
            events = events[events["NCTag"] >= 1]
            events = events[events["NBTag"] >= 1]
            events = events[events["NJet"] >= 3]
            events = events[events["nbaselineLep"] == 0]
            events = events[events["passMETtriggers"] == 1]
            events = events[events["met"] > 250]
            events = events[events["dphimin3"] > 0.3]            
            events = events[events["metSigST"] > 6] 
            events = events[events["MTcMin"] > 100]
            events = events[events["tcMeTCategory"] < 1 ]       # Use only tc+MET events for training (-1 is bkg, 0 is tc, 1 and 2 are tt and cc)
            events = events[events["isBTagJet1"] == 0]
            events = events[events["isCTagJet1"] == 0]            
            events = events[events["ptJet1"] > 100]
            events = events[events["ptJet4"] > 30]            
    
            logger.debug("n_events after pre-selection: " + str(len(events)))
        except: 
            logger.warning("0 events surviving pre-selection or problem reading Tree in file " + root_file)
            return None
    n_events = len(events)
    if n_events==0:
        logger.debug("Input has 0 events, ignoring file!")
        return None

    # Initialise parameters
    cont_parameters = torch.zeros(n_events, len(continuous_par_strs))
    disc_parameters = torch.LongTensor(n_events, len(discrete_par_strs)).zero_()

    # Loop over all parameters and get n_events from the Tree, fill it into the corresponding tensors 
    # Continuous parameters
    for i_parameter, parameter in enumerate(continuous_par_strs):
        cont_parameter = np.array(events[parameter])
        cont_parameters[:, i_parameter] = torch.as_tensor(cont_parameter[0:len(events)])
    # Discrete parameters
    for i_parameter, parameter in enumerate(discrete_par_strs):
        disc_parameter = np.array(events[parameter])
        disc_parameters[:, i_parameter] = torch.as_tensor(disc_parameter[0:len(events)]).type(torch.LongTensor)
    return cont_parameters, disc_parameters, disc_values, continuous_par_strs, discrete_par_strs 


def construct_dataset(inputs_list, label, continuous_par_strs, discrete_par_strs, device, class_names):
    cont_parameters_list, disc_parameters_list, disc_values_list = list(), list(), list()

    # Get data from all inputs
    for i, input in enumerate(inputs_list):
        cont_parameters_input, disc_parameters_input, disc_values_input, continuous_par_strs_input, discrete_par_strs_input = input

        # Take only used input variables from all the input variables in input (from the .pt file)
        cont_parameters_new = torch.zeros(len(cont_parameters_input), len(continuous_par_strs))
        for i_par, continuous_par_str in enumerate(continuous_par_strs):
            for j_par, continuous_par_str_input in enumerate(continuous_par_strs_input):
                if continuous_par_str_input == continuous_par_str:
                    cont_parameters_new[:, i_par] = cont_parameters_input[:, j_par]
        cont_parameters_list.append(cont_parameters_new)

        disc_parameters_new = torch.LongTensor(len(disc_parameters_input), len(discrete_par_strs)).zero_()
        for i_par, discrete_par_str in enumerate(discrete_par_strs):
            for j_par, discrete_par_str_input in enumerate(discrete_par_strs_input):
                if discrete_par_str_input == discrete_par_str:
                    disc_parameters_new[:, i_par] = disc_parameters_input[:, j_par]

                    if i==0:
                        disc_values_list.append(disc_values_input[j_par])
                    else:
                        disc_values_list[i_par] = np.unique( np.concatenate((disc_values_list[i_par], disc_values_input[j_par]), axis=0))
        disc_parameters_list.append(disc_parameters_new)

    # Concatenate all inputs together
    cont_parameters_tensor = torch.cat(cont_parameters_list, dim=0)
    disc_parameters_tensor = torch.cat(disc_parameters_list, dim=0)  

    label_tensor = torch.Tensor(len(cont_parameters_tensor)).zero_()
    label_tensor.fill_(label)
    label_tensor = label_tensor.type(torch.FloatTensor) if len(class_names)==2 else label_tensor.type(torch.LongTensor) # labels must be float for binary classifier and Long for multiclass classifier

    dataset = RootToDataset(label_tensor,cont_parameters_tensor, disc_parameters_tensor, continuous_par_strs, discrete_par_strs, disc_values_list, device)
    return dataset


def train(dataloader, optimizer, net, criterion, device, class_names):
    net.train()     # Sets the NN into training mode, using dropout layers and batch normalisation (if present)
    running_loss = torch.Tensor([0.0]).to(device)
    for i, sample in enumerate(dataloader):
        labels, cont_parameters, list_disc_parameters = sample[0], sample[1], sample[2]

        # Zero the parameter gradients
        optimizer.zero_grad()

        # Forward + backward + optimize
        output = net(cont_parameters, list_disc_parameters)
        loss = criterion(output, labels.unsqueeze(1) if len(class_names)==2 else labels)
        loss.backward()
        optimizer.step()

        # Print statistics
        running_loss += loss.item()*len(sample[0]) 
        del output, loss
    return running_loss.to("cpu")/len(dataloader)


def validate(net, dataloader, criterion, device, class_names):
    net.eval()     # Sets the NN into validation mode, not using dropout layers and batch normalisation
    with torch.no_grad():
        running_loss = torch.Tensor([0.0]).to(device)
        label_list, probabilities_list = list(), list()
        for sample in dataloader:
            labels, cont_parameters, list_disc_parameters = sample[0], sample[1], sample[2]
            output = net(cont_parameters, list_disc_parameters)
            loss = criterion(output, labels.unsqueeze(1) if len(class_names)==2 else labels)
            label_list.append(labels.cpu())

            if len(class_names)==2:
                probabilities = np.transpose(output)[0].cpu()
                probabilities = torch.sigmoid(probabilities)
                probabilities_list.append(probabilities)
            else:
                probabilities_list.append(output.cpu())                
            running_loss += loss.item()*len(sample[0])
        
            del output, loss

        if len(class_names) == 2:
            return running_loss.to("cpu")/len(dataloader), torch.cat(label_list, dim=0), torch.cat(probabilities_list, dim=0)
        else:
            return running_loss.to("cpu")/len(dataloader), torch.cat(label_list, dim=0), torch.cat(probabilities_list, dim=0)        


def get_stats(y_true, y_pred, class_names, cuts=[0.75]):

    from sklearn.metrics import confusion_matrix
    acc_classes = np.zeros((len(class_names)+1, len(cuts)))
    for i, cut in enumerate(cuts):
        y_pred_eval = y_pred
        if len(class_names) == 2:
            y_pred_eval = np.round(y_pred_eval - (cut - 0.5))
        else:
            y_pred_eval = torch.log_softmax(y_pred_eval, dim=1)
            _, y_pred_eval = torch.max(y_pred_eval, dim=1)
        labels = list()
        for i_class, class_name in enumerate(class_names):
            labels.append(i_class)
        cm = confusion_matrix(y_true, y_pred_eval, labels = labels)
        cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]    # Normalise confusion matrix, diagonal entries are the accuracies
        for i_class, class_name in enumerate(class_names):
            acc_classes[i_class][i] = cm_norm[i_class][i_class]
            if len(class_names) == 2:
                logger.info("%d\t out of %d\t %s correct, accuracy: %.3f %% (cut at %.2f)" % (cm[i_class][i_class], np.sum(cm[i_class][:]), class_name, acc_classes[i_class][i]*100.0, cut))           
            else:
                logger.info("%d\t out of %d\t %s correct, accuracy: %.3f %% (softmax)" % (cm[i_class][i_class], np.sum(cm[i_class][:]), class_name, acc_classes[i_class][i]*100.0))

            acc_classes[-1][i] += acc_classes[i_class][i]/len(class_names)
        if len(class_names) == 2:
            logger.info("%d\t out of %d\t total correct, avg. acc: %.3f %% (cut at %.2f)" % (np.trace(cm), np.sum(cm[:][:]), acc_classes[-1][i]*100.0, cut))        
        else:
            logger.info("%d\t out of %d\t total correct, avg. acc: %.3f %% (softmax)" % (np.trace(cm), np.sum(cm[:][:]), acc_classes[-1][i]*100.0))
    return acc_classes


### Plot neural net score(s) ###
def plot_score(
    probabilities_valid, probabilities_train, labels_valid, labels_train,
    epochs_valid, epochs_train,
    class_names,
    save_folder,
    str_valid="Validation", str_train="Training"):

    file= TFile.Open( save_folder + "plots.root","RECREATE")

    # Define histos
    discriminators = list()
    if len(class_names) == 2:
        discriminators.append("NN_score")
    else:
        for class_name in class_names:
            discriminators.append(class_name + "_score")

    h_probs_valid_list_all = list()
    h_probs_train_list_all = list()
    for discriminator in discriminators:
        h_probs_valid_list = list()
        h_probs_train_list = list()
        for i, class_name in enumerate(class_names):
            x_low = 0.0 if len(class_names)==2 else -2.0
            x_high = 1.0 if len(class_names)==2 else 2.0
            h_probs_valid = TH1D("%s discr. %s: Class %s" % (str_valid, discriminator, class_name),"%s discr. %s: Class %s" % (str_valid, discriminator, class_name) + "; Neural net score; Counts [norm.]", 50, x_low, x_high)
            h_probs_train = TH1D("%s discr. %s: Class %s" % (str_train, discriminator, class_name),"%s discr. %s: Class %s" % (str_train, discriminator, class_name) + "; Neural net score; Counts [norm.]", 50, x_low, x_high)
            h_probs_valid_list.append(h_probs_valid)
            h_probs_train_list.append(h_probs_train)
        h_probs_valid_list_all.append(h_probs_valid_list)
        h_probs_train_list_all.append(h_probs_train_list)

    # Fill histos
    for probs, label in zip(probabilities_valid, labels_valid):
        if len(class_names)==2:
            h_probs_valid_list_all[0][int(label)].Fill(probs)
        else:
            for i, prob in enumerate(probs):
                h_probs_valid_list_all[i][label].Fill(prob)
    for probs, label in zip(probabilities_train, labels_train):
        if len(class_names)==2:
            h_probs_train_list_all[0][int(label)].Fill(probs)
        else:
            for i, prob in enumerate(probs):
                h_probs_train_list_all[i][label].Fill(prob)

    # Make canvas and draw histograms
    p_values_list = list()

    for i_disc, discriminator in enumerate(discriminators): # One canvas per class in multiclass case, one canvas for binary classification
        c_discriminator = TCanvas(discriminator,discriminator,800,600 + 200 * len(class_names))
        c_discriminator.cd()

        # Chi2 test of training vs validation discriminator
        padTop = TPad("padTop " + discriminator,"padTop " + discriminator,0, 0.40 if len(class_names)==2 else 0.8, 1, 1.0)
        padTop.Draw()
        padTop.cd()

        p_values = list()
        for i, class_name in enumerate(class_names):
            h_probs_valid_list_all[i_disc][i].SetLineColor(colors[i])
            h_probs_train_list_all[i_disc][i].SetLineColor(colors[i])

            h_probs_valid_list_all[i_disc][i].SetLineStyle(linestyles[0])
            h_probs_train_list_all[i_disc][i].SetLineStyle(linestyles[0])
            h_probs_valid_list_all[i_disc][i].SetFillStyle(0)
#            h_probs_valid_list_all[i_disc][i].SetFillColorAlpha(colors[i], 0.5)
    
            h_probs_train_list_all[i_disc][i].SetMarkerStyle(20)
            h_probs_train_list_all[i_disc][i].SetMarkerColor(colors[i])
  
            # Calculate chi2 values
            res = np.empty([50], np.dtype('double'))
            p_value = h_probs_valid_list_all[i_disc][i].Chi2Test(h_probs_train_list_all[i_disc][i],"UU CHI2/NDF P",res);
            p_values.append(p_value)

            # Normalize histos
            valid_integral = h_probs_valid_list_all[i_disc][i].Integral() if h_probs_valid_list_all[i_disc][i].Integral()>0 else 1
            train_integral = h_probs_train_list_all[i_disc][i].Integral() if h_probs_train_list_all[i_disc][i].Integral()>0 else 1
            h_probs_valid_list_all[i_disc][i].Scale(1/valid_integral)
            h_probs_train_list_all[i_disc][i].Scale(1/train_integral)

        p_values_list.append(p_values)

        # Draw histos
        hist_max = 1.5*max([h.GetBinContent(h.GetMaximumBin()) for h in h_probs_valid_list_all[i_disc][:] + h_probs_train_list_all[i_disc][:]])
        h_probs_valid_list_all[i_disc][0].GetYaxis().SetRangeUser(0.0, hist_max)
        h_probs_valid_list_all[i_disc][0].Draw("hist")
        for h in h_probs_valid_list_all[i_disc][1:]:
            h.Draw("samehist")
        for h in h_probs_train_list_all[i_disc][:]:
            h.Draw("samehist E0 P")

        # legend
        leg = TLegend(0.20,0.70,0.83,0.92)
        leg.SetNColumns(2)
        leg.SetTextSize(0.040)
        leg.SetTextFont(42)
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)        
        for i, class_name in enumerate(class_names):
            leg.AddEntry(h_probs_valid_list_all[i_disc][i], class_name + ' ' + str_valid, 'l')
            leg.AddEntry(h_probs_train_list_all[i_disc][i], class_name + ' ' + str_train, 'pe')
        leg.Draw()

        padTop.Modified()

        # Ratio plots
        h_ratios = list()
        texts = list()
        legs = list()
        lines = list()
        for i_class, class_name in enumerate(class_names):
            hRatio = h_probs_valid_list_all[i_disc][i_class].Clone("hRatio " + class_name + str(discriminator))
            hRatio.Sumw2()
            hRatio.Divide(h_probs_train_list_all[i_disc][i_class])
            hRatio.SetMarkerStyle(20)
            hRatio.SetMarkerSize(1)
            hRatio.SetLineColor(colors[i_class])
            hRatio.SetMarkerColor(colors[i_class])
            hRatio.SetLineWidth(2)

            y = hRatio.GetYaxis()
            y.SetTitleSize(0.0)
            y.SetLabelSize(0.05)
            x = hRatio.GetXaxis()
            x.SetTitleSize(0.0)
            x.SetLabelSize(0.0)

            hRatio.GetYaxis().SetRangeUser(0.1,3.30)
            h_ratios.append(hRatio)
    
            # Draw p-values
            text = TLatex(0.5, 2.8, "#frac{#chi^{2}}{NDOF} = " + str(np.round(p_values_list[i_disc][i_class],5)))
            text.SetTextSize(0.1)
            text.SetTextFont(42)
            texts.append(text)

            # Legend
            legRatio = TLegend(0.25,0.85,0.50,0.92)
            legRatio.SetTextSize(0.10)
            legRatio.SetTextFont(42)
            legRatio.SetBorderSize(0)
            legRatio.SetFillStyle(0)
            legRatio.AddEntry(hRatio, "#frac{%s}{%s} %s" % (str_valid, str_train, class_name), 'pe')
            legs.append(legRatio)
    
            # Line at ratio 1
            line = TLine(0.0 if len(class_names)==2 else -2.0, 1, 1.0 if len(class_names)==2 else 2.0,1)
            line.SetLineWidth(3)
            lines.append(line)

        for i_class, class_name in enumerate(class_names):
            c_discriminator.cd()
            pad = TPad("pad " + class_name + str(discriminator),"pad " + class_name + str(discriminator), 0, 0.4-0.4/2*(i_class+1) if len(class_names)==2 else 0.8-0.8/len(class_names)*(i_class+1), 1, 0.4-0.4/2*i_class if len(class_names)==2 else 0.8-0.8/len(class_names)*(i_class))
            pad.SetTopMargin(0)
            pad.SetBottomMargin(0)
            pad.Draw()
            pad.cd()
            
            if i_class==0:
                h_ratios[i_class].Draw("e0 P0")
            else:
                h_ratios[i_class].Draw("same e0 P0")
            
            texts[i_class].Draw()
            c_discriminator.Update()
            pad.Modified()

            # Print Training/validation label
            legs[i_class].Draw()
            pad.Modified()

            # Print line
            lines[i_class].Draw()
            pad.Modified()
        c_discriminator.Print( save_folder + discriminator + "_" + str(epochs_valid[-1]) + str_valid + "_" + str_train + ".pdf")
        c_discriminator.Write()
        c_discriminator.Close()
    file.Close()
    return p_values_list

### Plot accuracy and loss evolution and precision-recall curve ###
def plot_acc( 
    epochs_valid, losses_valid, epochs_train, losses_train, 
    acc_classes_valid_list, acc_avg_valid_list,
    acc_classes_train_list, acc_avg_train_list,
    class_names,
    save_folder,
    str_valid="Validation", str_train="Training"):

    ### Loss and accuracy evolution plot
    c_evol = TCanvas("Evolution", "Evolution", 800, 800)
    c_evol.cd()
    padTop2 = TPad("padTop2","padTop2",0, 0.5, 1, 1)
    padTop2.Draw()
    padTop2.cd()
    mg = TMultiGraph()

    if len(epochs_train)>100:           # take only every 10th element not to overload plot
        epochs_train = epochs_train[9::10]
        losses_train = losses_train[9::10]

    # Legend
    leg2 = TLegend(0.63,0.72,0.83,0.92)
    leg2.SetTextSize(0.040)
    leg2.SetTextFont(42)
    leg2.SetBorderSize(0)
    leg2.SetFillStyle(0)

    # losses
    for i, (loss_type, epochs, losses) in enumerate(zip([str_valid + " loss", str_train + " loss"], [epochs_valid, epochs_train], [losses_valid, losses_train])):
        g_loss = TGraph(len(epochs), np.asarray(epochs, dtype=float), np.asarray(losses, dtype=float))
        g_loss.SetLineStyle(linestyles[i])
        g_loss.SetMarkerStyle(markerstyles[i])
        g_loss.SetMarkerSize(0.75)
        g_loss.SetLineWidth(2)
        mg.Add(g_loss, "lp")
        leg2.AddEntry(g_loss, loss_type, "pl")

    # Draw
    mg.Draw("alp")
    mg.GetXaxis().SetTitle("Epoch")
    mg.GetYaxis().SetTitle("Loss [a.u]`")
    x_max = mg.GetXaxis().GetXmax()
    leg2.Draw()
    padTop2.Modified()

    ### Accuracy plots
    c_evol.cd()
    padBottom2 = TPad("padBottom2","padBottom2",0.0, 0.0, 1, 0.5)
    padBottom2.Draw()
    padBottom2.cd()
    mg2 = TMultiGraph()   

    # Legend
    leg3 = TLegend(0.20,0.67,0.83,0.92)
    leg3.SetNColumns(2)
    leg3.SetTextSize(0.040)
    leg3.SetTextFont(42)
    leg3.SetBorderSize(0)
    leg3.SetFillStyle(0)
    
    graphs = list()
    graph_titles = list()
    for i, class_name in enumerate(class_names):
        g_acc_train = TGraph(len(epochs_valid), np.asarray(epochs_valid, dtype=float), np.asarray([acc[i] for acc in acc_classes_train_list], dtype=float))
        g_acc_valid = TGraph(len(epochs_valid), np.asarray(epochs_valid, dtype=float), np.asarray([acc[i] for acc in acc_classes_valid_list], dtype=float))
    
        g_acc_train.SetName(str_train + " " + class_name)
        g_acc_valid.SetName(str_valid + " " + class_name)

        g_acc_train.SetLineStyle(linestyles[1]); g_acc_train.SetMarkerStyle(markerstyles[1]);
        g_acc_valid.SetLineStyle(linestyles[0]); g_acc_valid.SetMarkerStyle(markerstyles[0]);

        g_acc_train.SetLineColor(colors[i]); g_acc_train.SetMarkerColor(colors[i]);
        g_acc_valid.SetLineColor(colors[i]); g_acc_valid.SetMarkerColor(colors[i]);

        g_acc_valid.SetMarkerSize(0.75)
        g_acc_train.SetMarkerSize(0.75)

        graphs.append(g_acc_valid)
        graphs.append(g_acc_train)
    
        graph_titles.append(str_valid + " " + class_name)
        graph_titles.append(str_train + " " + class_name)

    g_acc_train_avg = TGraph(len(epochs_valid), np.asarray(epochs_valid, dtype=float), np.asarray(acc_avg_train_list, dtype=float))
    g_acc_valid_avg = TGraph(len(epochs_valid), np.asarray(epochs_valid, dtype=float), np.asarray(acc_avg_valid_list, dtype=float))

    g_acc_train_avg.SetLineStyle(linestyles[1]); g_acc_train_avg.SetMarkerStyle(markerstyles[1]);
    g_acc_valid_avg.SetLineStyle(linestyles[0]); g_acc_valid_avg.SetMarkerStyle(markerstyles[0]);

    g_acc_train_avg.SetLineColor(colors[-1]); g_acc_train_avg.SetMarkerColor(colors[-1]);
    g_acc_valid_avg.SetLineColor(colors[-1]); g_acc_valid_avg.SetMarkerColor(colors[-1]);

    g_acc_valid_avg.SetMarkerSize(0.75)
    g_acc_train_avg.SetMarkerSize(0.75)
    
    graphs.append(g_acc_valid_avg)
    graphs.append(g_acc_train_avg)
    graph_titles.append(str_valid + " Average")
    graph_titles.append(str_train + " Average")

    for graph, title in zip(graphs, graph_titles):
        mg2.Add(graph)

    mg2.Draw("alp")
    for graph, title in zip(graphs, graph_titles):
        leg3.AddEntry(graph, title, "pl" )
    mg2.GetXaxis().SetTitle("Epoch")
    mg2.GetXaxis().SetRangeUser(0, x_max)
    mg2.GetYaxis().SetTitle("Accuracy")
    mg2.GetYaxis().SetRangeUser(0.0, 1.5)
    leg3.Draw()

    c_evol.Update()
    c_evol.Print( save_folder + "loss_and_accuracy_" + str_valid + "_" + str_train + ".pdf")
    file= TFile.Open( save_folder + "plots_" + str_valid + "_" + str_train + ".root","RECREATE")
    c_evol.Write()
    c_evol.Close()
    file.Close()


### Precision recall curve in binary classification ###
# Not implemented yet in multiclass classification!
def plot_precision_recall(
    probabilities_valid, probabilities_train, labels_valid, labels_train, 
    save_folder):

    c_pr = TCanvas("Precision-recall curves", "Precision-recall curves", 800, 600)
    c_pr.cd()
    mg_pr = TMultiGraph()

    from sklearn.metrics import precision_recall_curve, precision_recall_fscore_support
    precision_valid = dict()
    precision_train = dict()
    recall_valid = dict()
    recall_train = dict()
    threshold_valid = dict()
    threshold_train = dict() 

    pr_graphs = list()
    pr_titles = list()
    max_precision = 0.0
    max_recall = 0.0

    precision_valid, recall_valid, threshold_valid = precision_recall_curve(labels_valid, probabilities_valid) 
    precision_train, recall_train, threshold_train = precision_recall_curve(labels_train, probabilities_train)

    g_pr_valid = TGraph(len(precision_valid), np.asarray(precision_valid, dtype=float), np.asarray(recall_valid, dtype=float))
    g_pr_train = TGraph(len(precision_train), np.asarray(precision_train, dtype=float), np.asarray(recall_train, dtype=float))

    g_pr_valid.SetLineStyle(linestyles[0]); g_pr_valid.SetMarkerStyle(markerstyles[0]);
    g_pr_train.SetLineStyle(linestyles[1]); g_pr_train.SetMarkerStyle(markerstyles[1]);
    g_pr_valid.SetLineColor(colors[0]); g_pr_valid.SetMarkerColor(colors[0]);
    g_pr_train.SetLineColor(colors[1]); g_pr_train.SetMarkerColor(colors[1]);
    g_pr_valid.SetMarkerSize(0.50)
    g_pr_train.SetMarkerSize(0.50)

    pr_graphs.append(g_pr_valid)
    pr_graphs.append(g_pr_train)

    pr_titles.append("Validation dataset")
    pr_titles.append("Training dataset")

    max_precision = max(np.amax(np.concatenate((precision_valid, precision_train))), max_precision)
    max_recall = max(np.amax(np.concatenate((recall_valid, recall_train))), max_recall)

    for graph in pr_graphs:
        mg_pr.Add(graph, "lp")
    mg_pr.Draw("alp")
    mg_pr.GetXaxis().SetTitle("Precision")
    mg_pr.GetYaxis().SetTitle("Recall")
    mg_pr.GetXaxis().SetRangeUser(0.0, 1.2*max_precision)
    mg_pr.GetYaxis().SetRangeUser(0.0, 1.2*max_recall)
    c_pr.Update()

    leg_pr = TLegend(0.60,0.82,0.83,0.92)
    leg_pr.SetTextSize(0.040)
    leg_pr.SetTextFont(42)
    leg_pr.SetBorderSize(0)
    leg_pr.SetFillStyle(0)
    for graph, title in zip(pr_graphs, pr_titles):
        leg_pr.AddEntry(graph, title, "pl")
    leg_pr.Draw()
    c_pr.Update()
    c_pr.Print( save_folder + "precision-recall.pdf")
    file= TFile.Open( save_folder + "plots.root","RECREATE")
    c_pr.Write()
    c_pr.Close()
    file.Close()


def main(args):

    gROOT.SetBatch(args.quiet)
    gROOT.gErrorIgnoreLevel = ROOT.kWarning

    # Create new directory to save the plots
    save_folder = str(args.save_folder) + "/"
    if not os.path.isdir(save_folder):
        os.makedirs(save_folder, exist_ok = True)

    ### Logging ###
    try:
        logging.config.fileConfig('logging.conf', defaults={'logfilename': save_folder + "debug.log"})          # Running normally
    except:
        logging.config.fileConfig('/logging/logging.conf', defaults={'logfilename': save_folder + "debug.log"}) # Running in Docker or Singularity      

    # Print arguments
    logger.debug("Arguments:")
    with open(save_folder + "arguments.txt", "w") as file:
        for k in args.__dict__:
            logger.debug(str(k) + ": " + str( args.__dict__[k]))
            file.write(str(k) + ": " + str(args.__dict__[k]))

    ### Limit threads for the job
    num_threads = args.threads
    torch.set_num_threads(num_threads)
    logger.debug("\nUsed Threads:" + str(torch.get_num_threads()))

    ### Get GPU if available, otherwise take CPU ###
    device = get_device(args.gpu)

    ### Load/instantiate datasets ###
    dataset_loading_time = time.time()

    ### Get parameters ###
    continuous_par_strs = args.continuous_pars
    discrete_pars = args.discrete_pars
    logger.debug("\nUsed continuous parameters: " + str(continuous_par_strs) + "\nUsed discrete parameters: " + str(discrete_pars))

    ### Load data from .pt files saved in last iteration or reload from root files ###
    logger.debug("\nLoading data...")
    input_classes = load_json_input(args.input_json, continuous_par_strs, discrete_pars, pre_selection_par_strs)
    class_names = [input_class["name"] for input_class in input_classes]

    ### Construct datasets ###
    logger.debug("\nConstructing datasets...")
    label = 0
    for input_class in input_classes:
        for event_type in input_class["event_types"]:
            dataset = construct_dataset(event_type["data"], label, continuous_par_strs, list(discrete_pars), device, class_names) # Use copy of discrete_pars since this will be changed later
            event_type["dataset"] = dataset
        if sum(event_type["n_events"] for event_type in input_class["event_types"])>0:
            label = label + 1
    logger.debug("done!\n")


    ### Handle continuous parameters ##

    # Get normalisation from complete dataset
    dataset_total = None
    for input_class in input_classes:
        for event_type in input_class["event_types"]:
            if dataset_total == None:
                dataset_total = copy.deepcopy(event_type["dataset"])
            else:
                dataset_total += copy.deepcopy(event_type["dataset"])

    means, stds = dataset_total.get_normalisation(ignored_values=ignored_values)
    logger.debug("\nNormalising continuous input parameters:")
    for (parameter, mean, std) in zip(continuous_par_strs, means, stds): 
        logger.debug("\tParameter %s: Mean: %f, std: %f" % (parameter, mean, std) )

    # Get unique classes of the signal and background datasets and combine them
    logger.debug("\nLooking for unique discrete parameters...")
    discrete_pars = dataset_total.replace_missing_data(means, ignored_values=ignored_values)
    is_missing_strs = [s for s in discrete_pars if "_is_missing" in s]  # Relevant 'is_missing' strings    
    disc_classes_unique = dataset_total.get_disc_values()    
    for i, par in enumerate(discrete_pars):
        logger.debug("Unique values of parameter %s: %s" % (par,str(disc_classes_unique[i])) )
    del dataset_total 

    # Apply normalisation both to signal and to background dataset
    for input_class in input_classes:
        for event_type in input_class["event_types"]:
            event_type["dataset"].apply_normalisation(means, stds, ignored_values)

    # Handle missing values in continuous parameters, this adds discrete parameters <parameter name>_is_missing. Identical discrete parameters are not added (e.g ptJet4_is_missing is the same as etaJet4_is_missing)
    logger.debug("\nReplace missing data (values of " + str(ignored_values) + ") by mean")
    for input_class in input_classes:
        for event_type in input_class["event_types"]:
            _ = event_type["dataset"].replace_missing_data(means, ignored_values=ignored_values, default_missing_strs=is_missing_strs)


    ### Handle discrete parameters

    # Apply encoding to integers from 0 to n_classes-1
    logger.debug("\nApplying class encoding according to discrete parameter input list... (to ensure that all possible discrete parameter values can be embedded, all elements printed previously must be included in these lists")
    for input_class in input_classes:
        for event_type in input_class["event_types"]:
            event_type["dataset"].set_disc_values(disc_classes_unique)
            _ = event_type["dataset"].apply_class_encoding()

    ### Further dataset processing ###

    # Remove event classes with 0 events
    input_classes_to_remove = list()
    for input_class in input_classes:
        input_event_types_to_remove = list()
        for event_type in input_class["event_types"]:
            if event_type["n_events"] == 0:
                input_event_types_to_remove.append(event_type)
        
        for input_event_type_to_remove in input_event_types_to_remove:
            input_class["event_types"].remove(input_event_type_to_remove)
            logger.warning("Remove event type %s" % input_event_type_to_remove["name"])        

        if len(input_class["event_types"]) == 0:
            input_classes_to_remove.append(input_class)

    for input_class_to_remove in input_classes_to_remove:
        input_classes.remove(input_class_to_remove)
        logger.warning("Remove class %s" % input_class_to_remove["name"])

    class_names = [input_class["name"] for input_class in input_classes]

    # Split into trainset and testset, trainset will later be split into k-folds for actual training and validation sets
    datasets_train, datasets_test= list(), list()
    n_events_train_list, n_events_test_list = len(input_classes) * [0], len(input_classes) * [0]

    logger.debug("\nReducing the dataset sizes now")
    for label, input_class in enumerate(input_classes):
        for event_type in input_class["event_types"]:
            dataset_len_orig = len(event_type["dataset"])
            dataset_len_train = int(min(dataset_len_orig*(1-args.split), event_type["n_events"]*(1-args.split)))
            dataset_len_test = int(min(dataset_len_orig*(args.split), event_type["n_events"]*(args.split)))  
            dataset_train, dataset_test, _ = event_type["dataset"].random_split([dataset_len_train, dataset_len_test, dataset_len_orig-dataset_len_train-dataset_len_test], generator=torch.Generator().manual_seed(42)) # Fixing the random splitting to ensure reproducability
            logger.info("Event type %s: Using %d events for training, %d for testing out of %d events" % (event_type["name"], dataset_len_train, dataset_len_test, dataset_len_orig))
            datasets_train.append(dataset_train)
            datasets_test.append(dataset_test)
            n_events_train_list[label] += dataset_len_train 
            n_events_test_list[label] += dataset_len_test
    dataset_train = sum(datasets_train)
    dataset_test = sum(datasets_test)

    # Define k-fold validation from sklearn, using stratified k-fold validator to ensure same class splits among folds
    # If nFolds=1 then no k-fold cross validation is done and 20% of the training data are used for validation (without different folds)
    kfold = StratifiedKFold(n_splits=args.nFolds if args.nFolds>1 else 5, shuffle=True, random_state=42)    # Fixed random generator seed for reproducability, always same splits

    # Lists for saving the results of the various splits
    losses_test_folds, acc_test_folds, acc_avg_test_folds, chi2_test_folds  = list(), list(), list(), list()
    losses_valid_folds, acc_valid_folds, acc_avg_valid_folds, epochs_total, chi2_valid_folds  = list(), list(), list(), list(), list()

    # Using k-fold cross validation, split the training set into samples for training the NN and samples for validating the NN with nFolds folds
    for fold, (train_ids, valid_ids) in enumerate(kfold.split(dataset_train, dataset_train.labels)):

        logger.debug("Fold " + str(fold) + " running now!")

        # Create new directory to save the plots
        save_folder_fold = save_folder + "fold" + str(fold) + "/"
        if not os.path.isdir(save_folder_fold):
            os.makedirs(save_folder_fold, exist_ok = True)

        # Define data samplers
        train_subsampler = torch.utils.data.SubsetRandomSampler(train_ids, generator=torch.Generator().manual_seed(42)) # Also here fixed random seed
        valid_subsampler = torch.utils.data.SubsetRandomSampler(valid_ids, generator=torch.Generator().manual_seed(42))

        # Instantiate data loaders
        drop_last = True if len(train_ids) % args.batch_size == 1 else False   # drop the last batch if it has a size of 1, this would make batchnorm layer fail
        trainloader = torch.utils.data.DataLoader(dataset_train, batch_size = args.batch_size, num_workers = args.num_workers, drop_last = drop_last, pin_memory=False, sampler=train_subsampler)
        validloader = torch.utils.data.DataLoader(dataset_train, batch_size = len(valid_ids), num_workers = args.num_workers, pin_memory=False, sampler=valid_subsampler)

        dataset_loading_time = time.time()- dataset_loading_time
        logger.debug("\nDataset loading time: %f s" % dataset_loading_time)


        ### Define neural net ###
        net = Net(len(continuous_par_strs), disc_classes_unique, args.lin_layer_sizes, args.dropout_probs, args.embedding_multiplicity, len(class_names), args.activation_function, args.layer_order, args.no_batch_norm).to(device)
        net.weight_reset()

        if args.model != "":
            net, _, _ = torch.load(args.model + "model.pt")
        logger.debug("\nNeural net architecture: " + str(net) + "\n")
    
    
        ### Define a Loss function and optimizer ###
        optimizer = None
        if args.optimizer == "SGD":
            optimizer = torch.optim.SGD(net.parameters(), lr=args.learning_rate, momentum=args.momentum)   # Stochastic gradient descent 
        elif args.optimizer == "Adam":
            optimizer = torch.optim.Adam(net.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)   
        else:
            logger.critical("Optimizer " + args.optimizer + " is not implemented!")
            exit()

        # Weight classes to be able to use more background events
        class_imbalances = torch.ones(len(class_names))
        criterion = None    
        for i, (n_events_train, class_weight) in enumerate(zip(n_events_train_list, [input_class["weight"] for input_class in input_classes])):
            class_imbalances[i] = max(n_events_train_list)/(n_events_train)*class_weight
    
        # Use BCEWithLogitsLoss for two classes
        if len(class_names)==2:
            logger.info("Calculate loss using BCEWithLogitsLoss, weight signal more than background by: " + str(class_imbalances[1].cpu().numpy()) + "\n")
            criterion = torch.nn.BCEWithLogitsLoss(pos_weight=class_imbalances[1].to(device))
        # Use CrossEntropyLoss for more than two classes
        else:
            logger.info("Calculate loss using CrossEntropyLoss")        
            for class_name, class_imbalance in zip(class_names, class_imbalances):
                logger.info("Weight class %s by %f \n" % (class_name, class_imbalance.cpu().numpy()))            
            criterion = torch.nn.CrossEntropyLoss(weight=class_imbalances.to(device))
    
        ### Training ###
        training_time = time.time()
        epochs_train, losses_train, epochs_valid, losses_valid = list(), list(), list(), list()
    
        # Discriminator value distributions (overwrite always so that only the values from last epoch are saved)
        probabilities_valid, probabilities_train, labels_valid, labels_train = list(), list(), list(), list()
    
        # Number of correct signals/backgrounds
        acc_classes_valid_list, acc_avg_valid_list, acc_classes_train_list, acc_avg_train_list = list(), list(), list(), list()
        
        # Chi2 test results
        chi2_list = list()

        # Variables for early stopping
        epochs_no_improve = 0
        min_loss_valid = np.Inf
        early_stopping = False
    
        for epoch in  range(1,args.n_epochs+1):  # loop over the dataset multiple times
    
            # Do early stopping
            if early_stopping==True:
                break
    
            # Do one training step
            running_loss_train = train(trainloader, optimizer, net, criterion, device, class_names)/args.batch_size
            epochs_train.append(epoch)
            losses_train.append(running_loss_train.detach())

            with torch.no_grad():

                # Validate to get validation loss
                loss_valid, labels_valid, probabilities_valid = validate(net,validloader, criterion, device, class_names)
                loss_valid = loss_valid/len(valid_ids)

                # Save model and reset epoch counter if validation loss is at a new minimum    
                if loss_valid < min_loss_valid: # Early stopping taken from https://www.kaggle.com/akhileshrai/tutorial-early-stopping-vanilla-rnn-pytorch
                    epochs_no_improve = 0
                    min_loss_valid = loss_valid
                    torch.save([net, optimizer, class_names], save_folder_fold + "model.pt") # Save best model
                else:
                    epochs_no_improve += 1
            
                # Do early stopping if loss hasn't improved for early_stopping_epochs or if chi^2/ndof of last score on last sample (signal score on signal sample) is bigger than 2.5 (take the sample and score with the least amount of training data)
                if(epoch > 10 and epochs_no_improve == args.early_stopping_epochs):
                    logger.warning("Early stopping in epoch %d due to no further decrese of loss" % (epoch))
                    early_stopping = True
                elif( epoch > 10 and chi2[-1][-1]>2.5):
                    logger.warning("Early stopping in epoch %d due to chi2/ndof of %f" % (epoch,chi2[-1][-1]))
                    early_stopping = True
        
                # Calculate accuracies and chi2 for the first epoch, if the validation loss has improved or every 10 epochs if no early stopping is done
                if(epoch==1 or epochs_no_improve==0 or (epoch%10==0 and args.early_stopping_epochs==9999)):
                    logger.info('\n\nEpoch %d out of %d, loss: %.3f' % (epoch, args.n_epochs, losses_train[-1]))
        
                    # Validate the network on validation dataset every 10 epochs and at the end of training
                    logger.info("Accuracy on validation dataset")
                    accuracies_valid = get_stats(labels_valid, probabilities_valid, class_names, [0.5])
                    acc_classes_valid_list.append(accuracies_valid[0:len(class_names)][:])
                    acc_avg_valid_list.append(accuracies_valid[-1][:])
        
                    logger.info("Accuracy on training dataset")
                    loss, labels_train, probabilities_train = validate(net,trainloader, criterion, device, class_names)
        
                    accuracies_train = get_stats(labels_train, probabilities_train, class_names, [0.5])
                    acc_classes_train_list.append(accuracies_train[0:len(class_names)][:])
                    acc_avg_train_list.append(accuracies_train[-1][:])
        
                    losses_valid.append(loss_valid)
                    epochs_valid.append(epoch)
        
                    chi2 = plot_score(
                        probabilities_valid, probabilities_train, labels_valid, labels_train,
                        epochs_valid, epochs_train,
                        class_names,
                        save_folder_fold
                    )
                    logger.info("chi2/ndof of signal score on signal sample: " + str(chi2[-1][-1]) + "\n")
                    chi2_list.append(chi2)

        # Store information about the best model 
        epochs_total.append(epochs_valid[-1])
        losses_valid_folds.append(losses_valid[-1])
        acc_valid_folds.append(acc_classes_valid_list[-1])
        acc_avg_valid_folds.append(acc_avg_valid_list[-1])
        chi2_valid_folds.append(chi2_list[-1])

        ### Save best model ###
        net, _, _ = torch.load(save_folder_fold + "model.pt") # recover best model
        torch.save([continuous_par_strs, means, stds], save_folder_fold + "continuous_parameters.pt")
        torch.save([discrete_pars, disc_classes_unique, is_missing_strs, ignored_values], save_folder_fold + "discrete_parameters.pt")

        ### Further performance stats ###
        with torch.no_grad():

            ### NN performance for various cuts after all epochs ###
            cuts = list()
            if len(class_names) == 2:
                cuts = [x * 0.1 for x in range(1,10)]
            else:
                cuts = [0.75]
            logger.info("\nAccuracies for different cuts on training dataset:")
            accuracies_train = get_stats(labels_train, probabilities_train, class_names, cuts)
            acc_classes_train, acc_avg_train = accuracies_train[0:len(class_names)][:], accuracies_train[-1][:]
        
            logger.info("\nAccuracies for different cuts on validation dataset:")
            accuracies_valid = get_stats(labels_valid, probabilities_valid, class_names, cuts)
            acc_classes_valid, acc_avg_valid = accuracies_valid[0:len(class_names)][:], accuracies_valid[-1][:]
        
            ### Plotting ###
            logger.info("\nStarting plotting...")
            plot_acc(
                epochs_valid, losses_valid, epochs_train, losses_train,
                acc_classes_valid_list, acc_avg_valid_list,
                acc_classes_train_list, acc_avg_train_list,
                class_names,
                save_folder_fold
            )
            logger.info("Plotting done!")
        
            if len(class_names)==2:
                plot_precision_recall(
                    probabilities_valid, probabilities_train, labels_valid, labels_train,
                    save_folder_fold
                )
            
            ### Calculate performance on test dataset of this fold
            logger.info("Performance of fold " + str(fold) + " on testing dataset")

            loss_test, labels_test, probabilities_test = None, None, None
            if args.split > 0.0:   # Testing set
                testloader = torch.utils.data.DataLoader(dataset_test, batch_size = len(dataset_test), num_workers = args.num_workers, pin_memory=False)
                loss_test, labels_test, probabilities_test = validate(net,testloader, criterion, device, class_names)
                loss_test = loss_test/len(dataset_test)
            else:                  # Use Validation set instead of testing set if no testing set exists
                loss_test, labels_test, probabilities_test = validate(net,validloader, criterion, device, class_names)
                loss_test = loss_test/len(valid_ids)

            accuracies_test = get_stats(labels_test, probabilities_test, class_names, cuts)
            chi2 = plot_score(
                probabilities_test, probabilities_train, labels_test, labels_train,
                [0], epochs_train,
                class_names,
                save_folder_fold,
                str_valid="Testing", str_train="Training"
            )
            logger.info("chi2/ndof of signal score on signal sample: " + str(chi2[-1][-1]) + "\n")

            acc_test_folds.append(accuracies_test[0:len(class_names)][:])
            acc_avg_test_folds.append(accuracies_test[-1][:])
            losses_test_folds.append(loss_test)
            chi2_test_folds.append(chi2)

            plot_acc(
                [0], [loss_test], epochs_train, losses_train,
                acc_test_folds, acc_avg_test_folds,
                acc_classes_train_list, acc_avg_train_list,
                class_names,
                save_folder_fold,
                str_valid="Testing", str_train="Training"
            )



            ### Calculate importance of input variables
            logger.info("\nCalculating input variable importances\nBaseline loss after training: " + str(losses_valid[-1].item()))
            variable_loss_changes, variable_acc_changes = list(), list()
            for parameter_str in discrete_pars + continuous_par_strs:
                logger.info("\nVariable: %s" % (parameter_str))

                # Create new dataset with one parameter shuffled among events
                loader, dataset_shuffled = None, None
                batch_size_importances = 0
                if args.split > 0.0:   # Testing set
                    dataset_shuffled = copy.deepcopy(dataset_test)
                    dataset_shuffled.shuffle_parameter(parameter_str) 
                    batch_size_importances = len(dataset_shuffled)
                    loader = torch.utils.data.DataLoader(dataset_shuffled, batch_size = batch_size_importances, num_workers = args.num_workers, pin_memory=False)
                else:                  # Use Validation set instead of testing set if no testing set exists
                    dataset_shuffled = copy.deepcopy(dataset_valid)
                    dataset_shuffled.shuffle_parameter(parameter_str)
                    batch_size_importances = len(valid_ids)
                    loader = torch.utils.data.DataLoader(dataset_shuffled, batch_size = batch_size_importances, num_workers = args.num_workers, pin_memory=False, sampler=valid_subsampler)

                # Calculate loss and accuracy
                loss_importance, labels_importance, probabilities_importance = validate(net, loader, criterion, device, class_names)
                loss_importance = loss_importance/batch_size_importances
                accuracies_importance = get_stats(labels_importance, probabilities_importance, class_names)
                logger.info("Loss: %f, \tloss changed by %f%% avg. accuracy changed by %f%%" % (loss_importance.item(), loss_importance.item()/losses_valid[-1].item() * 100.0-100.0, (-acc_avg_valid_list[-1][0]+accuracies_importance[-1])*100.0))
                variable_loss_changes.append(loss_importance.item()/losses_valid[-1].item() * 100.0-100.0)
                variable_acc_changes.append((-float(acc_avg_valid_list[-1][0])+float(accuracies_importance[-1]))*100.0)
        
                del dataset_shuffled

            # Write importances 
            df2 = pd.DataFrame({
                'variable' : discrete_pars + continuous_par_strs,
                'loss change [%]': [str(loss) for loss in variable_loss_changes],
                'accuracy change [abs. %]': [str(acc) for acc in variable_acc_changes]
            })
            df2.to_csv(save_folder_fold + "variable_importances.csv", index=False, mode='w')


        ### Needed if no k-fold cross validation is to be done ###
        if args.nFolds==1:
            break
            
    training_time = time.time()-training_time
    logger.debug("Training time: %f s" % training_time)
    
    ### Writing to csv ###
    if os.path.exists(args.output_file):
        with open(args.output_file, "r") as file:
            isempty = len(file.read()) < 1
    else:
        isempty = True

    # Hyperparameters
    df = pd.DataFrame({
        'save folder': [save_folder],
        'linear layer sizes': [str(args.lin_layer_sizes)],
        'batch_size': [args.batch_size],
        'dropout_prob': [str(args.dropout_probs)],
        'layer_order': [args.layer_order],
        'activation_function': [args.activation_function],
        'no_batch_norm': [args.no_batch_norm],
        'embedding_multi': [args.embedding_multiplicity],
        'learning rate': [args.learning_rate],
        'momentum' if args.optimizer=="SGD" else 'weight_decay': [args.momentum] if args.optimizer=="SGD" else [args.weight_decay]}
    )

    # Number of training events and further hyperparameters
    for (class_name, n_events) in zip(class_names, n_events_train_list):
        df = pd.concat([df,pd.DataFrame({'n_events_train_%s' % class_name: [n_events]})], axis=1)

    # Number of testing events
    for (class_name, n_events_valid) in zip(class_names, n_events_test_list):
        df = pd.concat([df, pd.DataFrame({'n_events_test_' + class_name: [n_events_valid]})], axis=1)

    # Results on validation set
    for i_class, class_name in enumerate(class_names):
        for iFold in range(args.nFolds):
            df = pd.concat([df, pd.DataFrame({'Valid acc %s fold %d ' % (class_name, iFold): ['%.3f' % acc_valid_folds[iFold][i_class]]})], axis=1)
        if args.nFolds>1:
            df = pd.concat([df, pd.DataFrame({'Valid acc %s mean' % (class_name): ['%.3f' % np.mean(acc_valid_folds[:][i_class])]})], axis=1)
            df = pd.concat([df, pd.DataFrame({'Valid acc %s std' % (class_name): ['%.3f' % np.std(acc_valid_folds[:][i_class])]})], axis=1)
    for iFold in range(args.nFolds):
        df = pd.concat([df, pd.DataFrame({'Valid acc avg fold %d ' % (iFold): ['%.3f' % acc_avg_valid_folds[iFold]]})], axis=1)
    if args.nFolds>1:
        df = pd.concat([df, pd.DataFrame({'Valid acc avg mean': ['%.3f' % np.mean(acc_avg_valid_folds[:])]})], axis=1)
        df = pd.concat([df, pd.DataFrame({'Valid acc avg std': ['%.3f' % np.std(acc_avg_valid_folds[:])]})], axis=1)
    for iFold in range(args.nFolds):
        df = pd.concat([df, pd.DataFrame({'Valid loss fold %d ' % (iFold): ['%.3f' % losses_valid_folds[iFold]]})], axis=1)
    if args.nFolds>1:
        logger.warning(str(type(losses_valid_folds)))
        logger.warning(str(type(losses_valid_folds[0])))
        df = pd.concat([df, pd.DataFrame({'Valid loss mean': ['%.3f' % torch.mean(torch.stack(losses_valid_folds))]})], axis=1)
        df = pd.concat([df, pd.DataFrame({'Valid loss std': ['%.3f' % torch.std(torch.stack(losses_valid_folds))]})], axis=1)
    for i_class, i_class_name in enumerate(class_names):
        for j_class, j_class_name in enumerate(class_names):
            for iFold in range(args.nFolds):
                df = pd.concat([df, pd.DataFrame({'Valid chi2/ndof %s score, %s sample, fold %d' % (i_class_name, j_class_name, iFold): [chi2_valid_folds[iFold][i_class][j_class]]})], axis=1)
            if args.nFolds>1:
                df = pd.concat([df, pd.DataFrame({'Valid chi2/ndof %s score, %s sample, mean' % (i_class_name, j_class_name): ['%.3f' % np.mean(chi2_valid_folds, axis=0)[i_class][j_class]]})], axis=1)
                df = pd.concat([df, pd.DataFrame({'Valid chi2/ndof %s score, %s sample, std' % (i_class_name, j_class_name): ['%.3f' % np.std(chi2_valid_folds, axis=0)[i_class][j_class]]})], axis=1)

    # Results on testing set 
    for i_class, class_name in enumerate(class_names):
        for iFold in range(args.nFolds):
            df = pd.concat([df, pd.DataFrame({'Test acc %s fold %d ' % (class_name, iFold): ['%.3f' % acc_test_folds[iFold][i_class]]})], axis=1)
        if args.nFolds>1:
            df = pd.concat([df, pd.DataFrame({'Test acc %s mean' % (class_name): ['%.3f' % np.mean(acc_test_folds[:][i_class])]})], axis=1)    
            df = pd.concat([df, pd.DataFrame({'Test acc %s std' % (class_name): ['%.3f' % np.std(acc_test_folds[:][i_class])]})], axis=1)        
    for iFold in range(args.nFolds):
        df = pd.concat([df, pd.DataFrame({'Test acc avg fold %d ' % (iFold): ['%.3f' % acc_avg_test_folds[iFold]]})], axis=1)
    if args.nFolds>1:
        df = pd.concat([df, pd.DataFrame({'Test acc avg mean': ['%.3f' % np.mean(acc_avg_test_folds[:])]})], axis=1)
        df = pd.concat([df, pd.DataFrame({'Test acc avg std': ['%.3f' % np.std(acc_avg_test_folds[:])]})], axis=1)
    for iFold in range(args.nFolds):
        df = pd.concat([df, pd.DataFrame({'Test loss fold %d ' % (iFold): ['%.3f' % losses_test_folds[iFold]]})], axis=1)
    if args.nFolds>1:
        df = pd.concat([df, pd.DataFrame({'Test loss mean': ['%.3f' % torch.mean(torch.stack(losses_test_folds))]})], axis=1)
        df = pd.concat([df, pd.DataFrame({'Test loss std': ['%.3f' % torch.std(torch.stack(losses_test_folds))]})], axis=1)
    for i_class, i_class_name in enumerate(class_names):
        for j_class, j_class_name in enumerate(class_names):
            for iFold in range(args.nFolds):
                df = pd.concat([df, pd.DataFrame({'Test chi2/ndof %s score, %s sample, fold %d' % (i_class_name, j_class_name, iFold): [chi2_test_folds[iFold][i_class][j_class]]})], axis=1) 
            if args.nFolds>1:
                df = pd.concat([df, pd.DataFrame({'Test chi2/ndof %s score, %s sample, mean' % (i_class_name, j_class_name): ['%.3f' % np.mean(chi2_test_folds, axis=0)[i_class][j_class]]})], axis=1)
                df = pd.concat([df, pd.DataFrame({'Test chi2/ndof %s score, %s sample, std' % (i_class_name, j_class_name): ['%.3f' % np.std(chi2_test_folds, axis=0)[i_class][j_class]]})], axis=1)

    # Epochs until end and computation times
    for iFold in range(args.nFolds):
        df = pd.concat([df, pd.DataFrame({'Epochs fold %d ' % (iFold): ['%d' % epochs_total[iFold]]})], axis=1)        
    df = pd.concat([df, pd.DataFrame({
        't_loading': [dataset_loading_time],
        't_train': [training_time]})],
        axis=1
    )

    # Input parameters used
    df = pd.concat([df, pd.DataFrame({
        'disc_parameters': [str(discrete_pars)],
        'cont_parameters': [str(continuous_par_strs)]})],
        axis=1
    )
    df = pd.concat([df, pd.DataFrame({
        'json_file': [args.input_json.name]})],
        axis=1
    )    
    df.to_csv(args.output_file, index=False, header=isempty, mode='a')

    ### Cleanup
    del datasets_train, datasets_test
    torch.cuda.empty_cache()
    logger.info("Finished!")


if __name__ == "__main__":
    # Argument parsing
    args = parse_arguments(sys.argv[1:])

    # Go to main function
    main(args)
