# General seetings and instantiations
run_list=()
maxJobs=$(nproc --all)  # Get maximum number of simultaneous threads
maxJobs=$((maxJobs -2)) # Leave one thread empty to handle overhead


# Variables to loop over
batch_size_list=(128)
dropout_prob_list=("0.2 0.5")
lin_layer_sizes_list=("96 96") #
#"64" "128") # "256" "512" "1024" "2048"
             #         "64 64" "128 128" "256 256" "512 512" "1024 1024"
              #        "42 42 42" "85 85 85" "171 171 171" "341 341 341" "683 683 683"
               #       "32 32 32 32" "64 64 64 64" "128 128 128 128" "256 256 256 256"
                #      )

#                      "128" "256" "512"
#                      "64 64" "256 256"
#                      "85 85 85"
#                     )

#                      "128" "256" "512" "1024" "2048"
#                      "64 64" "128 128" "256 256" "512 512" "1024 1024"
#                      "42 42 42" "85 85 85" "171 171 171" "341 341 341" "683 683 683")
                      
#                      "96" "128" "192" "256" "384" "512" "768" "1024" "2048"
#                      "48 48" "64 64" "96 96" "128 128" "192 192" "256 256" "384 384" "512 512" "1024 1024"
#                      "32 32 32" "42 42 42" "64 64 64" "85 85 85" "128 128 128" "171 171 171" "256 256 256" "341 341 341" "683 683 683")
#                      "24 24 24 24" "32 32 32 32" "48 48 48 48" "64 64 64 64" "96 96 96 96" "128 128 128 128" "192 192 192 192" "256 256 256 256" "512 512 512 512"
#                      "16 16 16 16 16 16" "21 21 21 21 21 21" "32 32 32 32 32 32" "43 43 43 43 43 43" "64 64 64 64 64 64" "85 85 85 85 85 85" "128 128 128 128 128 128" "171 171 171 171 171 171" "341 341 341 341 341 341"
#)

embedding_depth_list=(0.1)
weight_decay_list=(3e-3)
learningrate_list=(1e-3) # 1e-4 3e-5)
continuous_pars=("ptJet1 ptJet2 ptJet3 ptJet4 ptJet5 ptJet6 etaJet1 etaJet2 etaJet3 etaJet4 etaJet5 etaJet6 dPhiJet1 dPhiJet2 dPhiJet3 dPhiJet4 dPhiJet5 dPhiJet6 ptBJet1 dPhiBJet1 etaBJet1 ptCJet1 dPhiCJet1 etaCJet1 ptBJet2 dPhiBJet2 etaBJet2 ptCJet2 dPhiCJet2 etaCJet2 met NJet NBTag NCTag") # "ptJet1 ptJet2 ptJet3 ptJet4 ptJet5 ptJet6 etaJet1 etaJet2 etaJet3 etaJet4 etaJet5 etaJet6 ptBJet1 dPhiBJet1 etaBJet1 ptCJet1 dPhiCJet1 etaCJet1 ptBJet2 dPhiBJet2 etaBJet2 ptCJet2 dPhiCJet2 etaCJet2 met" "ptJet1 ptJet2 ptJet3 ptJet4 ptJet5 ptJet6 etaJet1 etaJet2 etaJet3 etaJet4 etaJet5 etaJet6 ptBJet1 dPhiBJet1 etaBJet1 ptCJet1 dPhiCJet1 etaCJet1 ptBJet2 dPhiBJet2 etaBJet2 ptCJet2 dPhiCJet2 etaCJet2 metSigST" "ptJet1 ptJet2 ptJet3 ptJet4 ptJet5 ptJet6 etaJet1 etaJet2 etaJet3 etaJet4 etaJet5 etaJet6 ptBJet1 dPhiBJet1 etaBJet1 ptCJet1 dPhiCJet1 etaCJet1 ptBJet2 dPhiBJet2 etaBJet2 ptCJet2 dPhiCJet2 etaCJet2 meff") # met metSigST MTcMin MTbMin dRj1j2 dRb1c1 meff MTjMin mctbc mctjj"
discrete_pars=("flavJet2 flavJet3 flavJet4 flavJet5 flavJet6") # NBTag NCTag flavJet2 flavJet3 flavJet4 flavJet5 flavJet6"
layer_orders=("LABD") #"LADB" "LABD" "LBAD" "LDAB" "LDBA")
no_batch_norms=("")
activation_functions=("LeakyReLU") # "LeakyReLU0.04" "LeakyReLU0.08" "Sigmoid") # "ReLU")
jsonfiles=("inputs_multiclass_1.0_onlyFlavCont_ptJet4_152signals.json") # "inputs_binary.json"
savefolder="multiclass_properlyNormalised2/test"
iJob=0

repetition=1
split=(0.2)

mkdir -p $savefolder
mkdir -p $savefolder/logs

for i in $(seq 1 $repetition); do
for jsonfile in "${jsonfiles[@]}"; do
for no_batch_norm in "${no_batch_norms[@]}"; do
for activation_function in "${activation_functions[@]}"; do
for layer_order in "${layer_orders[@]}"; do
for continuous_par in "${continuous_pars[@]}"; do
for batch_size in "${batch_size_list[@]}"; do
    for dropout_prob in "${dropout_prob_list[@]}"; do
        for lin_layer_sizes in "${lin_layer_sizes_list[@]}"; do
            for embedding_depth in "${embedding_depth_list[@]}"; do
                for weight_decay in "${weight_decay_list[@]}"; do
                    for learning_rates in "${learningrate_list[@]}"; do
                        iJob=$((iJob+1))
                        run_string="nohup python Classifier.py -i ${jsonfile} --nFolds 5 --split ${split} -e 1 -n ${batch_size} -l ${lin_layer_sizes} -d ${dropout_prob} -m ${embedding_depth} --learning_rate ${learning_rates} --weight_decay ${weight_decay} --continuous_pars ${continuous_par} --discrete_pars ${discrete_pars} -o ${savefolder}/results.csv -q --early_stopping_epochs 100 --layer_order ${layer_order} --activation_function=${activation_function} ${no_batch_norm} -sf ${savefolder}/${iJob} > ${savefolder}/logs/${iJob}.log 2>&1 &"
                        run_list+=("$run_string")
                    done
                done 
            done
        done
    done
done
done
done
done
done
done
done

echo $run_list


iJob=0
echo "$nJobs"
for run_string in "${run_list[@]}"; do
    iJob=$((iJob+1))

    runningJobs=$(jobs | wc -l | xargs)     # Get the number of jobs already started
    while [ "$runningJobs" -ge "$maxJobs" ]; do
        runningJobs=$(jobs | wc -l | xargs)     # Get the number of jobs already started
        sleep 1
    done
    echo Job number $iJob out of ${#run_list[@]} running now
    eval "${run_string}"
done

runningJobs=$(jobs | wc -l | xargs)     # Get the number of jobs already started
while [ "$runningJobs" -gt 1 ]; do
    runningJobs=$(jobs | wc -l | xargs)     # Get the number of jobs already started
    echo "$runningJobs jobs left"
    sleep 10
done

echo "All jobs done!"
