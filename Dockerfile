# Using pytorch/pytorch container as base for build
FROM continuumio/miniconda3

# Conda already included in pytorch/pytorch base image, installing additional tools
RUN conda install -y python=3.7 && \
    conda install -y pytorch -c pytorch && \
    conda install -c conda-forge uproot && \
    conda install -c conda-forge root && \
    conda install scikit-learn && \
    conda install -c anaconda pandas && \
    conda clean -tipsy

WORKDIR /app

# The code to run when container is started:
COPY Classifier.py /app/Classifier.py
COPY RootToDataset.py /app/RootToDataset.py
COPY Net.py /app/Net.py
COPY logging.conf /logging/logging.conf
COPY inputs_multiclass_1.0_onlyFlavCont_ptJet4_152signals.json /app/inputs_multiclass_1.0_onlyFlavCont_ptJet4_152signals.json
# Copy tensor files (.pt) into the container
# COPY sn_test.pt /data/sn_test.pt
# COPY bg_test.pt /data/bg_test.pt

ENV PATH="/opt/conda/bin:$PATH"
ENV PYTHONPATH $PWD
CMD ["conda", "run", "-n", "myenv", "/bin/bash", "-c", "/app/Classifier.py"]
